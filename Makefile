AVAILABLE_FOLDERS=$(wildcard */Dockerfile)
RULES=$(AVAILABLE_FOLDERS:/Dockerfile=)

IMAGE_NAME=shadowcker
BUILD_ARGS=--force-rm --build-arg VIDEO_GID=$$(cat /etc/group | grep video | cut -d: -f3) --build-arg INPUT_GID=$$(cat /etc/group | grep input | cut -d: -f3)
DUAL_SCREEN=false
SHELL=/bin/sh

run: beta start

start:
	docker-compose up --scale shadowcker=$$([ "${DUAL_SCREEN}" != "false" ] && echo 2 || echo 1)

stop:
	docker-compose kill

${RULES}:
	docker build -t ${IMAGE_NAME} ${BUILD_ARGS} $@

clean:
	docker-compose rm
	docker rmi $(IMAGE_NAME) || true

.PHONY: ${RULES}
